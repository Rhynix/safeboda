<?php
use Illuminate\Http\Request;
#auth routes
Route::post('/auth/login', 'Auth\AuthController@login');
Route::post('/auth/logout', 'Auth\AuthController@logout');
Route::get('/auth/password/generate', 'Auth\AuthController@generatePassword');
Route::post('/auth/password/reset', 'Auth\ForgotCredentialsController@resetPassword');
Route::post('/auth/register', 'Auth\AuthController@store');

## Messages Routes
Route::group(['middleware' => 'jwt.auth'], function () {
    // Routes that are only for authenticated users.
    Route::get('/allpromos', 'Promocodes\PromocodesController@index');
    Route::post('/newcodes', 'Promocodes\PromocodesController@store');
    Route::post('/validitycode', 'Promocodes\PromocodesController@validitytest');
    Route::put('/promocode/{id}/update', 'Promocodes\PromocodesController@update');
    Route::post('/promocodevalidity', 'Promocodes\PromocodesController@promocodevalidity');
    
    Route::get('/alleventlist', 'Events\EventsController@alleventlist');
    Route::get('/allevent', 'Events\EventsController@index');
    Route::put('/event/{id}/update', 'Events\EventsController@update');
    Route::post('/newevent', 'Events\EventsController@store');
    Route::get('/getevent/{id}', 'Events\EventsController@show');
    Route::get('/searchvenue/{search}', 'Events\EventsController@searchvenue');
    
    
    
    
    
    

    Route::post('/newproductsize', 'Products\ProductsController@storesize');
    Route::put('/product/{id}/update', 'Products\ProductsController@update');
    Route::get('/getproduct/{id}', 'Products\ProductsController@show');
    Route::post('/product/image', 'Products\ProductsController@upload');
    Route::get('/searchproduct/{search}', 'Products\ProductsController@searchproduct');

    Route::get('/auth/user', 'Auth\AuthController@user');
    Route::post('/user/register', 'Users\UserController@store');
    Route::put('/users/{id}/update','Users\UserController@update');
    Route::get('/users/{id}', 'Users\UserController@show');
    Route::get('/users', 'Users\UserController@index');
    Route::delete('/user/delete/{id}','Users\UserController@delete');
    Route::get('/usercount', 'Users\UserController@usercount');
    
    Route::get('/allsizes', 'Sizes\SizesController@index');
    Route::post('/newsize', 'Sizes\SizesController@store');
    Route::get('/sizelist', 'Sizes\SizesController@sizelist');

    Route::post('/validate', 'Reports\P001Controller@validateDates');
    Route::post('/P001pdf', 'Reports\P001Controller@P001pdf');
    Route::post('/P002pdf', 'Reports\P002Controller@P002pdf');
});
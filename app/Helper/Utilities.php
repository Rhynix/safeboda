<?php

namespace App\Helper;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Callbacks\Callback;
/**
*
*/
class Utilities {

	public static function generatePassword ()
	{
		function randVowel()
		{
			$vowels = array("a", "e", "i", "o", "u");
			return $vowels[array_rand($vowels, 1)];
		}

		function randConsonant()
		{
			$consonants = array('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'z');
			return $consonants[array_rand($consonants, 1)];
		}

		function generatePIN($digits = 4){
			$i = 0; //counter
			$pin = ""; //our default pin is blank.
			while($i < $digits){
				//generate a random number between 0 and 9.
				$pin .= mt_rand(0, 9);
				$i++;
			}
			return $pin;
		}

		function rand_word(){
			return ucfirst(randConsonant() .  randVowel() .  randConsonant() .  randVowel()) . generatePIN();
		}

		return rand_word();
	}

	// public static function weeklyDataFormatter($callBacks, $currentDate, $currentWeek)
	// {
	//   $mon = $tue = $wed = $thur = $fri = $sat = array();
	//   $weekly = array();

	//   foreach($callBacks as $callback)
	//   {
	//     foreach ($callBacks as $calls)
	//     {
	//       if (Carbon::parse($calls->time)->format('W') == $currentWeek) {
	//         switch (Carbon::parse($calls->time)->dayOfWeek) {
	//           case Carbon::MONDAY:
	//             if (!in_array($calls->time, $mon)) {
	//                 array_push($mon, Callback::findOrFail($calls->id));
	//             }
	//             break;
	//           case Carbon::TUESDAY:
	//             if (!in_array($calls->time, $tue)) {
	//                 array_push($tue, Callback::findOrFail($calls->id));
	//             }
	//             break;
	//           case Carbon::WEDNESDAY:
	//             if (!in_array($calls->time, $wed)) {
	//                 array_push($wed, Callback::findOrFail($calls->id));
	//             }
	//             break;
	//           case Carbon::THURSDAY:
	//             if (!in_array($calls->time, $thur)) {
	//                 array_push($thur, Callback::findOrFail($calls->id));
	//             }
	//             break;
	//           case Carbon::FRIDAY:
	//             if (!in_array($calls->time, $fri)) {
	//                 array_push($fri, Callback::findOrFail($calls->id));
	//             }
	//             break;
	//           case Carbon::SATURDAY:
	//             if (!in_array($calls->time, $sat)) {
	//                 array_push($sat, Callback::findOrFail($calls->id));
	//             }
	//             break;
	//           default:
	//             break;
	//         }
	//       }
	//     }
	//     $weekly = [
	//         'mon' => $mon, 'tue' => $tue, 'wed' => $wed,
	//         'thur' => $thur,'fri' => $fri, 'sat' => $sat
	//     ];
	//     $mon = $tue = $wed = $thur = $fri = $sat = array();
	//   }

	//   function datesOfWeek($currentDate)
	//   {
	//     $dates = array();
	//     $date = Carbon::parse($currentDate)->startOfWeek();

	//     for($day = 0; $day < 6; $day++)
	//     {
	//       if ($day == 0) {
	//           $dates["mon"] = $date->addDay(0)->format("d");
	//       } else if ($day == 1) {
	//           $dates["tue"] = $date->addDay(1)->format("d");
	//       } else if ($day == 2) {
	//           $dates["wed"] = $date->addDay(2)->format("d");
	//       } else if ($day == 3) {
	//           $dates["thur"] = $date->addDay(3)->format("d");
	//       } else if ($day == 4) {
	//           $dates["fri"] = $date->addDay(4)->format("d");
	//       } else if ($day == 5) {
	//           $dates["sat"] = $date->addDay(5)->format("d");
	//       } else {
	//           break;
	//       }
	//       $date = Carbon::parse($currentDate)->startOfWeek();
	//     }
	//     return $dates;
	//   }
	//   return ['weekly' => $weekly,'dates' => datesOfWeek($currentDate)];
	// }

	// Check Users Roles As Per The Actions To Taken
	// 1. Check Roles For Supervisor or Admin Before Approving Or Rejecting an Overtime
	public static function checkForSupervisor()
	{
		return request()->user()->role_id == 2 OR request()->user()->role_id == 1;
	}

	public static function dateTimeValidator($date)
	{
		$format = "d-m-Y HH:MM";
		$date = Carbon::parse($date);
		$date->format($format);
		if ($date->dayOfWeek == Carbon::SUNDAY)
		{
			return "callback cannot be scheduled for sunday!";
		}
		return $date->gt(Carbon::now()) ? 1 : 0;
	}
}

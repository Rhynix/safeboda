<?php

namespace App\Helper;

class EventDefinitions {

	public const UPDATE 			= "Record update";
	public const DELETE 			= "Record delete";
	public const LOGIN 				= "User login";
	public const LOGOUT 			= "User logout";
	public const PASSRESET 		= "Password reset";
	public const NEWUSER 			= "New user added";

	# Event Types
	public const USEREVENT = 'User event';
	public const SYSTEMEVENT = 'System event';
}

<?php

namespace App\Http\Controllers\Promocodes;

use App\Http\Requests\Promocodes\UpdatePromocodesFormRequest;
use App\Http\Requests\Promocodes\PromocodesFormRequest;
use App\Models\Promocodes\Promocodes;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests\Promocodes\PromocodesRouteFormRequest;
// use Cornford\Googlmapper\Mapper;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use App\Http\Requests\Promocodes\PromocodeValidityFormRequest;
use App\Models\Events\Events;

class PromocodesController extends Controller
{
    public function index(Request $request)
    {
        $products = Promocodes::allcodes();
        return response()->json([
            'data' => $products,
            'error' => false,
        ], 201);
    }

    public function activecodes(Request $request)
    {
        $products = Promocodes::activecodes();
        return response()->json([
            'data' => $products,
            'error' => false,
        ], 201);
    }

    public function inactivecodes(Request $request)
    {
        $products = Promocodes::inactivecodes();
        return response()->json([
            'data' => $products,
            'error' => false,
        ], 201);
    }

    public function usedcodes(Request $request)
    {
        $products = Promocodes::usedcodes();
        return response()->json([
            'data' => $products,
            'error' => false,
        ], 201);
    }

    public function deactivatedcodes(Request $request)
    {
        $products = Promocodes::deactivatedcodes();
        return response()->json([
            'data' => $products,
            'error' => false,
        ], 201);
    }
    

    public function show($id)
    {
        $issue = Promocodes::with(["eventdetail","userdetail"])->where('id',$id)->first();
        if ($issue) {
            return response()->json([
                "data" => $issue,
                "error" => false
            ], 200);
        }else{
            return response()->json(['invalid' => "The Promo code does not exist","error" => true], 422);
        }
    }

    public function validatepromo(PromocodesRouteFormRequest $request)
    {
        $code = $request->promoCode;
        $issue = Promocodes::where('promocode',$code)->first();
        dd($issue);
        if ($issue) {
            return response()->json([
                "data" => $issue,
                "error" => false
            ], 200);
        }else{
            return response()->json(['invalid' => "The Promo code does not exist","error" => true], 422);
        }
    }

    public function getDistance( $latitude1, $longitude1, $latitude2, $longitude2 ) {

        // //Kiambu
        // $latitude1 = -1.16667;
        // $longitude1 = 36.83333;

        // //Nairobi
        // $latitude2= -1.28333;
        // $longitude2= 36.81667;
        // $radius = 50;

        $earth_radius = 6371;
        $dLat = deg2rad( $latitude2 - $latitude1 );  
        $dLon = deg2rad( $longitude2 - $longitude1 );
        
        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);  
        $c = 2 * asin(sqrt($a));  
        $d = $earth_radius * $c;
        return $d;
    }

    public function validitytest(PromocodeValidityFormRequest $request)
	{
        $longOrigin = $request->longOrigin;
        $latOrigin = $request->latOrigin;
        $event = Events::where('venue',$request->venue)->first();

        if ($event) {
            $longDestination = $event->long;
            $latDestination = $event->lat;
            $pcode = Promocodes::where('promocode',$request->promoCode)->with(["eventdetail","userdetail"])->first();
            $distance = $this->getDistance( $latOrigin, $longOrigin, $latDestination, $longDestination);
            if( $distance < $pcode->radius ) {
                if(Carbon::parse($pcode->expirydate) >= Carbon::now()->format('Y-m-d')){
                    $map = Mapper::map($latDestination, $longDestination, ['zoom' => 8, 'markers' => ['title' => $event->venue, 'animation' => 'DROP']])->circle([['latitude' => $latDestination, 'longitude' => $longDestination]], ['strokeColor' => '#C43B3A', 'strokeOpacity' => 0.8, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF', 'radius' => $pcode->radius*1000])->polyline([['latitude' => $latOrigin, 'longitude' => $longOrigin], ['latitude' => $latDestination, 'longitude' => $longDestination]], ['strokeColor' => '#000000', 'strokeOpacity' => 0.5, 'strokeWeight' => 2])->render();
                    // dd($map);
                    return response()->json([
                        'mappolyline' => implode(' ', $map),
                        'promodata' => $pcode,
                        'error' => false,
                    ], 201);
                }else{
                    return response()->json([
                        'data' => "The promo code is expired!",
                        'error' => true,
                    ], 201);
                }
            }else {
                return response()->json([
                    'data' => "The location is outside $pcode->radius kilometer radius!",
                    'error' => true,
                ], 201);
            }
        }else{
            return response()->json([
                'data' => "The event does not exist!",
                'error' => true,
            ], 201);
        }
    }
    
    public function promocodevalidity(PromocodeValidityFormRequest $request)
	{
        $event = Events::where('venue',$request->venue)->first();
        
        if ($event) {
            $longOrigin = $request->origin;
            $venue = $event->venue;
            $json = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/directions/json?origin='.$longOrigin.'&destination='.$venue.'&mode=driving&key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU&libraries'), true);

            $data = $json['routes'][0]['legs'][0]['steps'];

            for($i = 0; $i < count($data); ++$i) {
                $mpesacost[] = $data[$i]['start_location'];
                $mpesacost[] = $data[$i]['end_location'];
            }

            $overview_polyline = $json['routes'][0]['overview_polyline'];

            $endLat = $json['routes'][0]['bounds']['northeast']['lat'];
            $endLog = $json['routes'][0]['bounds']['northeast']['lng'];

            $startLat = $json['routes'][0]['bounds']['southwest']['lat'];
            $startLog = $json['routes'][0]['bounds']['southwest']['lng'];

            $origin = ['lat'=>$startLat,'lng'=>$startLog];
            $destination = ['lat'=>$endLat,'lng'=>$endLog];
            $map = [$origin, $destination];
            $end = $endLat.','.$endLog;
            $start = $startLat.','.$startLog;

            $pcode = Promocodes::where('promocode',$request->promoCode)->with(["eventdetail","userdetail"])->first();
            $distance = $this->getDistance( $startLat, $startLog, $endLat, $endLog);
            if( $distance < $pcode->radius ) {
                if(Carbon::parse($pcode->expirydate) >= Carbon::now()->format('Y-m-d')){
                    return response()->json([
                        'data'=> [
                            'promodata' => $pcode,
                            'polyline' => $mpesacost,
                            'map' => $map,
                            'end' => $end,
                            'overview_polyline' => $overview_polyline,
                            'start' => $start,
                            'error' => false,
                        ]
                    ], 201);
                }else{
                    return response()->json([
                        'data' => "The promo code is expired!",
                        'error' => true,
                    ], 201);
                }
            }else {
                return response()->json([
                    'data' => "The location is outside $pcode->radius kilometer radius!",
                    'error' => true,
                ], 201);
            }
        }else{
            return response()->json([
                'data' => "The event does not exist!",
                'error' => true,
            ], 201);
        }
    }

    public function deactivatecode(UpdatePromocodesFormRequest $request, $id)
    {
        $pcode = Promocodes::where('id',$id)->where('promocode',$request->promoCode)->first();
        if ($pcode) {
            $pcode->update([
                'status'    => 2,
            ]);
            return response()->json(['data'=>$pcode, 'error' => false], 200);
        }else{
            return response()->json(['data'=>"The Promo code does not exist", 'error' => true], 422);
        }
    }

    public function randCode()
    {
        $vowels = strtoupper(Promocodes::randconsonant() . Promocodes::randvowel()) . Promocodes::generatepin() . strtoupper(Promocodes::randconsonant() .  Promocodes::randvowel()) . Promocodes::generatepin2() . strtoupper(Promocodes::randconsonant() . Promocodes::randvowel());
        return $vowels;
    }

    public function store(PromocodesFormRequest $request)
    {
        $count = $request->count;
        $expirydate = (!empty($request->expirydate) ? Carbon::parse($request->expirydate)->format('Y-m-d') : null);
        for ($i = 1; $i <= $count; $i++) {
            $product = Promocodes::create([
                'event_id'		=> $request->event,
                'promocode'     => $this->randCode(),
                'amount'        => $request->amount,
                'radius'  		=> $request->radius,
                'expirydate'    => $expirydate,
                'status'  		=> 0
            ]);
        }
        return response()->json([
            'data' => "Codes created",
            'error' => false,
        ], 201);
    }

    public function update(UpdatePromocodesFormRequest $request, $id)
    {
        $product = Promocodes::findOrFail(intval($id));
        $expirydate = (!empty($request->expirydate) ? Carbon::parse($request->expirydate)->format('Y-m-d') : null);
        $product->update([
            'amount'    => $request->amount,
            'radius'    => $request->radius,
            'expirydate'=> $expirydate
        ]);
        return response()->json(['data'=>$product, 'error' => false], 200);
    }

    public function destroy($id)
    {
        $issue = Promocodes::findOrFail($id);
        $issue->delete();
        return response()->json([
            'data' => 'Product size deleted successfully.',
            'error' => false
        ], 200);
    }

    public function searchproduct(Request $request)
    {
        $search = $request->search;
        if($search != null)
        {
            $product = Promocodes::where('categoryname', 'LIKE', '%'.$search.'%')
            ->orWhere(function ($query) use ($search) {
                $query->where('description', 'LIKE', '%'.$search.'%');
            })->orWhere(function ($query) use ($search) {
                $query->where('price', 'LIKE', '%'.$search.'%');
            })->orderBy("id", "DESC")->paginate(25);
            return response($product);
        }
    }
}

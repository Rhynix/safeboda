<?php

namespace App\Http\Controllers\Users;

use App\Models\User\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\UserEvent;
use App\Http\Requests\User\UserUpdateFormRequest;
use App\Helper\EventDefinitions;
use Carbon\Carbon;
use App\Http\Requests\Auth\RegisterFormRequest;
use App\Http\Requests\User\UpdateTokenFormRequest;

class UserController extends Controller
{
	public function index(Request $request)
	{
		$users = User::get();
		return response()->json(['data' => $users], 200);
	}

	public function usercount(Request $request)
    {
        $usercount = User::where('status', 1)->count();
        return response()->json(['data' => $usercount ], 201);
    }

	public function show(Request $request)
	{
		$user = User::where('id',$request->id)->first();
		return response()->json(['data' => $user], 200);
	}

  	public function update(UserUpdateFormRequest $request, $id)
  	{
		$user = User::findOrFail($id);
		if ($request->role == "Admin") {
			$code = 1;
		}else{
			$code = 3;
		}
	  	if ($user) {
		  	$user->firstname  	= $request->firstName;
		  	$user->lastname   	= $request->lastName;
		  	$user->email    	= $request->email;
		  	$user->role_id      = $code;
		  	$user->save();
		  	return response()->json([
			  'data' => $user,
			  'status' => 'success'
		  	], 200);
	  	}
	  	return response()->json([
		  	'data' => "User not found",
		  	'status' => 'failure'
	  	], 400);
	}

	public function store(RegisterFormRequest $request)
    {
		if ($request->role_id == "Admin") {
			$code = 1;
		}else{
			$code = 3;
		}
        $user = User::create([
            'firstname' => $request->firstName,
            'lastname'  => $request->lastName,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
            'role_id'  => $code,
            'status'  => 1
        ]);
        return response()->json([
            'data' => $user,
            'error' => false,
        ], 200);
    }
	public function delete($id)
	{
		$user = User::destroy(intval(request()->id));
		return response()->json([
			'data' => 'User deleted successfully'
		], 200);
	}
}

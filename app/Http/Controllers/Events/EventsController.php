<?php

namespace App\Http\Controllers\Events;

use App\Http\Controllers\Controller;
use App\Http\Requests\Events\EventsFormRequest;
use App\Http\Requests\Events\UpdateEventsFormRequest;
use App\Transformers\EventsTransformer;
use App\Models\Events\Events;
use Illuminate\Http\Request;
use App\Models\User\User;
use Carbon\Carbon;
use Mail;
use Log;

class EventsController extends Controller
{
    public function index(Request $request)
    {
        $orders = Events::allevents();
        return response($orders);
    }

    public function show($id)
    {
        $Events = Events::findOrFail(intval($id));
        if ($Events) {
            return response()->json([
                "data" => $Events,
                "error" => false
            ], 200);
        }else{
            return response()->json(['data' => "The Events does not exist!","error" => true], 422);
        }
    }

    public function alleventlist(Request $request)
    {
      $Events = Events::allevents();
      return fractal()
           ->collection($Events)
           ->transformWith(new EventsTransformer)
           ->toArray();
    }

    public function store(EventsFormRequest $request)
    {
        $neworder = Events::create([
            'venue'   => $request->venue,
            'added_by'=> request()->user()->id
        ]);
        return response()->json(['data' => $neworder ], 201);
    }

    public function update(UpdateEventsFormRequest $request, $id)
    {
        $event = Events::findOrFail(intval($id));
        $event->update([
            'venue'   => $request->venue
        ]);
        return response()->json(['data'=>$event,'error' => false], 200);
    }

    public function searchvenue(Request $request)
    {
        $search = $request->search;
        if($request->search != null)
        {
            $event = Events::with(["userdetails"])->where('venue', 'LIKE', '%'.$search.'%')->get();
            return response($event);
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Events\Mailing\Mailer;
use App\Events\UserEvent;
use App\Helper\{Utilities,EventDefinitions};
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\{RegisterFormRequest, LoginFormRequest};
use App\Mail\UserCredentials;
use App\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Exceptions\{JWTException};
use Tymon\JWTAuth\JWTAuth;
use Carbon\Carbon;

class AuthController extends Controller
{
    protected $auth;

    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
    }

    public function login(LoginFormRequest $request)
    {
        try{
            if (!$token = $this->auth->attempt(['email' => $request->email, 'password' => $request->password, 'status' => 1])) {
                return response()->json([
                    'errors' => ['root' => 'Incorrect Credentials or Account Suspended'],
                    'error' => true,
                ], 401);
            }
        } catch (JWTException $e) {
            return response()->json([
                'errors' => ['root' => 'invalid credentials'],
                'error' => true,
            ], $e->getStatusCode());
        }

        return response()->json([
            'data' => $request->user(),
            'meta' => ['token' => $token],
            'error' => false,
        ], 200);
    }

    public function store(RegisterFormRequest $request)
    {
        $user = User::create([
            'firstname' => $request->firstName,
            'lastname'  => $request->lastName,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
            'role_id'  => $request->role_id,
            'status'  => 1
        ]);
        return response()->json([
            'data' => $user,
            'error' => false,
        ], 200);
    }

    public function generatePassword()
    {
      return response()->json([
        'data' => [
            'password' => Utilities::generatePassword()
        ]
      ], 200);
    }

    public function logout(Request $request)
    {
    	$this->auth->invalidate($this->auth->getToken());

        return response(null, 200);
    }

    public static function user(Request $request)
    {
        return response()->json([
            'data' => $request->user()
        ]);
    }
}

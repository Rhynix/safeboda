<?php

namespace App\Http\Controllers\Auth;

use App\Events\Mailing\Mailer;
use App\Events\UserEvent;
use App\Helper\EventDefinitions;
use App\Helper\Utilities;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Mail\UserCredentials;
use App\Models\User\User;
use Carbon\Carbon;
use Mail;
use App\Mail\PasswordResetMail;
use Illuminate\Http\Request;

class ForgotCredentialsController extends Controller
{
    public function resetPassword(ResetPasswordRequest $request) {
        $newPassword = Utilities::generatePassword();
        $user = User::where('email',$request->email)->first();
        $user->password = bcrypt($newPassword);
        $user->save();
        $this->updatedpassword($user, $newPassword);
        return response()->json([
            'data' => 'Your new password has been sent to the email provided'
        ], 200);
    }

    public function updatedpassword ($user, $password) {
        $password = empty($password) ? config('app.adminpass') : $password;
        $today = date("F j Y, g:i a");
        try {
            Mail::to($user->email)->later(10, new PasswordResetMail($user, $password, $today));
        } catch (Swift_TransportException $e) {
            Log.info("Mail not sent, reasons: ".$e->getMessage());
        }
    }

}

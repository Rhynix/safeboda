<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\FormRequest;

class RegisterFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required|min:3',
            'lastName'  => 'required|min:3',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|min:6',
            'role_id'  => 'required',
        ];
    }
}

<?php

namespace App\Http\Requests\Promocodes;

use App\Http\Requests\FormRequest;

class UpdatePromocodesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'radius' => 'required',
            'amount' => 'required',
            'expirydate' => 'required|date',
        ];
    }
}

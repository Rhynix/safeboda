<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

		if ($request->user()->role_id === 3) {
    		return response()->json([
                'errors' => [
                    'root' => 'Need higher priviledges.'
                ]
            ], 403);
		}

        return $next($request);
    }
}

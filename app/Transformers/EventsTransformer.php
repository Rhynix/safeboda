<?php
	namespace App\Transformers;

	use App\Models\Events\Events;

	class EventsTransformer extends \League\Fractal\TransformerAbstract
	{
		public function transform (Events $Events)
		{
			return[
				'lable' => $Events->id,
				'value' => $Events->venue
			];
		}
	}
?>
<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Messages\Messages;

class NewClientMail extends Mailable
{
    use Queueable, SerializesModels;
    public $clientmessage;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Messages $clientmessage)
    {
        $this->clientmessage = $clientmessage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->clientmessage->subject)
                    ->replyTo($this->clientmessage->email, $this->clientmessage->fullname)
                    ->from($this->clientmessage->email, $this->clientmessage->fullname)
                    ->view('emails.newClientMail');
    }
}

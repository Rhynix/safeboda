<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User\User;

class PasswordResetMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $user, $password, $today;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $password, $today)
    {
        $this->user = $user;
        $this->password = $password;
        $this->today = $today;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("PHOTOzuri New Password")->view('emails.newpassword');
    }
}

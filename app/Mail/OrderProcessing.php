<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Orders\Orders;

class OrderProcessing extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $user, $order, $dateFrom;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $order, $dateFrom)
    {
        $this->user = $user;
        $this->order = $order;
        $this->dateFrom = $dateFrom;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("PHOTOzuri Order being processed")->view('emails.orderprocessing');
    }
}

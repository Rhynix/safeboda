<?

namespace App\Observers\User;

use App\Models\User\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUserMail;

class UserObserver
{

    public function created (User $user) {
        $password = empty(request()->password) ? config('app.adminpass') : request()->password;
        try {
            Mail::to($user)->queue(new NewUserMail($user, $password));
        } catch (Exception $e) {
            \Log::info("Mail not sent, reasons: ".$e->getMessage());
        }
    }
}

<?php

namespace App\Models\Promocodes;

use Illuminate\Database\Eloquent\Model;
use App\Models\Events\Events;
use App\Models\User\User;
use Carbon\Carbon;

class Promocodes extends Model
{
    protected $fillable = [
        'event_id', 'promocode', 'amount', 'users_id', 'radius', 'expirydate', 'status'
    ];

    protected $appends = ['statusname','daysleft'];

    public function getStatusnameAttribute(){
        if ($this->status == 0) {
            //new and has never been used
            return "Not used";
        }elseif($this->status == 1){
            //currently being in use
            return "Active";
        }elseif($this->status == 2){
            //deactivated by a user
            return "Deactivated";
        }elseif($this->status == 3){
            //it has already been used
            return "Used";
        }else{
            return "Unkown";
        }
    }

    public function getDaysleftAttribute()
    {
        if(Carbon::parse($this->expirydate) >= Carbon::now()->format('Y-m-d')){
            return Carbon::parse($this->expirydate)->diffInDays(Carbon::now()) + 1;
        }else{
            return 0;
        }
    }

    public function scopeRandvowel()
    {
        $vowels = array("a", "e", "i", "o", "u");
        return $vowels[array_rand($vowels, 1)];
    }

    public function scopeRandconsonant()
    {
        $consonants = array('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'z');
        return $consonants[array_rand($consonants, 1)];
    }

    public function scopeGeneratepin(){
        $digits = 4;
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while($i < $digits){
            //generate a random number between 0 and 9.
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }

    public function scopeGeneratepin2(){
        $digits = 2;
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while($i < $digits){
            //generate a random number between 0 and 9.
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }

    public function eventdetail () {
        return $this->belongsTo(Events::class, 'event_id');
    }

    public function userdetail () {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function scopeAllcodes($query)
    {
    	return $query->with(["eventdetail","userdetail"])->paginate(100);
    }

    public function scopeActivecodes($query)
    {
    	return $query->with(["eventdetail","userdetail"])->where('status', 1)->paginate(100);
    }

    public function scopeInactivecodes($query)
    {
    	return $query->with(["eventdetail","userdetail"])->where('status', 0)->paginate(100);
    }

    public function scopeDeactivatedcodes($query)
    {
    	return $query->with(["eventdetail","userdetail"])->where('status', 2)->paginate(100);
    }

    public function scopeUsedcodes($query)
    {
    	return $query->with(["eventdetail","userdetail"])->where('status', 3)->paginate(100);
    }
    
}

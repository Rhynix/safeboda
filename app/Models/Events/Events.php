<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;
use App\Models\Promocodes\Promocodes;
use App\Models\User\User;
use Carbon\Carbon;

class Events extends Model
{
    protected $fillable = [
        'venue', 'lat', 'long', 'added_by'
    ];

    public function promocodes () {
        return $this->hasMany(Promocodes::class);
    }

    public function userdetails () {
        return $this->belongsTo(User::class, 'added_by');
    }

    public function scopeAllpromos($query)
    {
    	return $query->with(["promocodes","userdetails"])->orderBy("id", "DESC")->get();
    }

    public function scopeAllevents($query)
    {
    	return $query->with(["userdetails"])->get();
    }
}

<?php
namespace App\Models\User;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Promocodes\Promocodes;
use Carbon\Carbon;
use App\Models\Events\Events;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'role_id', 'status'
    ];

    protected $appends = ['full_name', 'role'];

    protected $hidden = [
        'password'
    ];

    public function promocodes () {
        return $this->hasMany(Promocodes::class);
    }

    public function eventsdesc () {
        return $this->hasMany(Events::class);
    }

    public function getRoleAttribute(){
        if ($this->role_id == 1) {
            return "Admin";
        }elseif($this->role_id == 2){
            return "Normal user";
        }else{
            return "Unkown";
        }
    }

    public function getFullNameAttribute(){
        return $this->firstname." ".$this->lastname;
    }
}

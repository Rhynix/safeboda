<?php

namespace App\Models\Audit;

use Illuminate\Database\Eloquent\Model;

class AuditLog extends Model
{
    protected $fillable = [
    	'user_id', 'ip_address', 'action', 'date', 'event_type'
    ];

    public $timestamps = false;

    protected $dates = ['date'];
}

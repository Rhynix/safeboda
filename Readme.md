Safe boda Case study.

Below are the features that are achieved in the application:

● Generation of new promo codes for events
● The promo code is worth a specific amount of ride
● The promo code can expire
● Can be deactivated
● Return active promo codes
● Return all promo codes
● Only valid when user’s pickup or destination is within x radius of the event venue
● The promo code radius should be configurable
● To test the validity of the promo code, expose an endpoint that accept origin, destination,
the promo code. The api should return the promo code details and a polyline using the destination and origin if promo code is valid and an error otherwise.

I have intergrated the API with a dashboad that a user can login and manipulate the data from the data.

The dashboad and the API are secured using tekens so one must log in first to use them

The languages used to buld the application are:

Laravel
Vue
Vuex
and intergration of google API for polyline responces


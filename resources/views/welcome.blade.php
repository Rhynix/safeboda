<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="../img/logo.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../img/logo.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../img/logo.png">
        <link type="text/css" href="/css/app.css" rel="stylesheet">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>SafeBoda</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta property="og:image" content="http://www.photozuri.com/img/logo.png" />
        <meta property="og:image:secure_url" content="http://www.photozuri.com/img/logo.png" />
        <meta property="og:image:width" content="222" />
        <meta property="og:image:height" content="192" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
                'keys' => [
                    'pusher' => config('broadcasting.connections.pusher.key')
                ]
            ]) !!};
        </script>
    </head>
    <body>
        <div id="app">
            <app></app>
        </div>
        {{--  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3.31&region=GB&language=en-gb&key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU&libraries=places"></script>  --}}
        <script src="http://maps.google.com/maps/api/js?libraries=geometry&amp;sensor=false"></script>
        <script type="text/javascript" src="/js/app.js"></script>
    </body>
</html>

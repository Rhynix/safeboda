// CommitChart.js 
import { Bar, Line } from 'vue-chartjs'
export default Line.extend({
    props: ['chart'],
    methods: {
        recievedata() {
            var vm = this.chart;
            this.gradient = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450)
            this.gradient2 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450)

            this.gradient.addColorStop(0, 'rgba(255, 0,0, 0.5)');
            this.gradient.addColorStop(0.5, 'rgba(255, 0, 0, 0.25)');
            this.gradient.addColorStop(1, 'rgba(255, 0, 0, 0)');
            
            this.gradient2.addColorStop(0, 'rgba(0, 231, 255, 0.9)')
            this.gradient2.addColorStop(0.5, 'rgba(0, 231, 255, 0.25)');
            this.gradient2.addColorStop(1, 'rgba(0, 231, 255, 0)');

            this.renderChart({
                labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                datasets: [
                    {
                        label: vm.LeadYear,
                        borderColor: '#05CBE1',
                        pointBackgroundColor: '#05CBE1',
                        borderWidth: 2,
                        pointBorderColor: 'white',
                        backgroundColor: this.gradient2, 
                        data: [vm.sJan, vm.sFeb, vm.sMar, vm.sApr, vm.sMay, vm.sJun, vm.sJul, vm.sAug, vm.sSep, vm.sOct, vm.sNov, vm.sDec]
                    },
                    {
                        label: vm.PrevYear,
                        borderColor: '#FC2525',
                        pointBackgroundColor: '#FC2525',
                        borderWidth: 2,
                        pointBorderColor: 'white',
                        backgroundColor: this.gradient,
                        data: [vm.pJan, vm.pFeb, vm.pMar, vm.pApr, vm.pMay, vm.pJun, vm.pJul, vm.pAug, vm.pSep, vm.pOct, vm.pNov, vm.pDec]
                    }
                ],
                options: {
                    // This chart will not respond to mousemove, etc
                    events: ['click']
                }
            },
            {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        barPercentage: 0.7,
                        ticks: {
                            // fontSize: 40,
                            fontColor: '#ffffff',
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            // fontSize: 40,
                            fontColor: '#ffffff',
                        }
                    }]
                },
                legend: {
                    labels: {
                        fontColor: '#ffffff',
                        boxWidth: 14
                    }
                },
                title: {
                    title: {
                        fontColor: '#ffffff'
                    }
                }
            })
        },
    },
    watch: {
        chart: function() {
            this.recievedata()
        }
    },
    mounted () {
        this.recievedata()
    }
})
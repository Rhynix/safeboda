import Bus from '../../../bus'

export const changeWeekDate = ({ commit }, payload) => {
    commit('changeWeekDate', payload)
}

export const fetchFuelData = ({ commit }, { payload }) => {
    return axios.get('/api/saleschart/search?search='+payload.date).then((response) => {
        commit('setAnualChartSalesData', response.data);
    })
}
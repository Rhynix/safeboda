import moment from 'moment'

export const PrevOrdersPage = (state, data) => {
    state.prevPage = data
}

export const setAnualChartSalesData = (state, data) => {
	state.salesdata = data
}

export const changeWeekDate = (state, data) => {
	state.weekDate = moment(data)
}

export const setOrdersMeta = (state, data) => {
	state.ordersmeta = data
}

export const selectedSize = (state, data) => {
	state.order = data
}

export const setsizelist = (state, data) => {
	state.sizelist = data
}


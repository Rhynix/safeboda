import moment from 'moment'

export const getsalesdata = (state) => {
    return state.salesdata
}

export const getprevPage = (state) => {
    return state.prevPage
}

export const getWeekDate = (state) => {
    return state.weekDate
}

export const getordersmeta = (state) => {
    return state.ordersmeta
}

export const getsize = (state) => {
    return state.order
}

export const getsizelist = (state) => {
    return state.sizelist
}
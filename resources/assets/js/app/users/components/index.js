import Vue from 'vue'

export const Users = Vue.component('users', require('./Users.vue'))
export const User = Vue.component('user', require('./User.vue'))
export const NewUser = Vue.component('new-user', require('./NewUser.vue'))
export const UserTableRow = Vue.component('user-table-row', require('./UserTableRow.vue'))

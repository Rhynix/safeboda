import { Users, User, NewUser } from '../components'

export default [
    {
        path: '/users',
        component: Users,
        name: 'users',
        meta: {
            needsAuth: true
        }
    },
    {
        path: '/new-user',
        component: NewUser,
        name: 'new-user',
        meta: {
            needsAuth: true
        }
    },
    { name: "user", path: '/user/:id', component: User, meta: { needsAuth: true }  }
]

import moment from 'moment'
import isEmpty from 'lodash'

export const users = (state) => {
    return state.users
}

export const user = (state) => {
    return state.user
}

export const createdAt = (state) => {
	return moment(state.user.created_at).fromNow()
}

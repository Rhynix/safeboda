import { isEmpty } from 'lodash'
import localforage from 'localforage'

export const fetchUsers = ({ commit, context }) => {
    return axios.get('/api/users').then((response) => {
        commit('setUsersData', response.data.data)
    }).catch((error) => {
        console.log(response);
        // error.response.status >= 500 ? context.statusCode = true : context.errors = error.response.data.errors
    })
}

export const fetchUser = ({ commit }, { payload }) => {
    return axios.get('/api/users/' + payload.id).then((response) => {
        commit('setSelectedUser', response.data.data)
    }).catch((error) => {
        console.log(response);
        // error.response.status >= 500 ? context.statusCode = true : context.errors = error.response.data.errors
    })
}

export const selectUser = ({ commit }, payload) => {
    commit('setUserData', payload)
}


export const updateUser = ({ dispatch }, { payload, context}) => {
    return axios.put('/api/users/' + payload.id + '/update', payload).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const deleteUser = ({ commit }, id) => {
    return axios.delete('/api/user/delete/' + id).then((response) => {
        commit('deletedUser', id)
    })
}

export const checkUserExists = ({ commit, dispatch }) => {
    return localforage.getItem('user').then((user) => {
        if (isEmpty(user)) {
            return Promise.reject('NO_USER');
        }
        return Promise.resolve(user)
    })
}

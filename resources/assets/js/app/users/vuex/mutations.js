import localforage from 'localforage'
import { isEmpty } from 'lodash'

export const setUsersData = (state, data) => {
	state.users = data
}

export const setSelectedUser = (state, data) => {
	state.user = data
}


export const deletedUser = (state, data) => {
	state.users = state.users.filter((user) => {
		return user.id !== data
	})
}

export const setUserData = (state, data) => {

	if (isEmpty(data)) {
		state.user = null
	}
	state.user = data
}

export const removeUserData = (state) => {
	state.user = {
		name: null,
		email: null,
		role_id: null,
		created_at: null
	}
}

export const checkUserData = (state) => {
	if (!isEmpty(localforage.getItem('user'))) {
		return
	}
}

import { Sizes } from '../components'

export default [
    {
        path: '/sizes',
        component: Sizes,
        name: 'sizes',
        meta: {
            needsAuth: true
        }
    }
]

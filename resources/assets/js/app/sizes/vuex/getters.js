import moment from 'moment'

export const getprevPage = (state) => {
    return state.prevPage
}

export const getorders = (state) => {
    return state.orders
}

export const getordersmeta = (state) => {
    return state.ordersmeta
}

export const getpromo = (state) => {
    return state.promo
}

export const getsizelist = (state) => {
    return state.sizelist
}
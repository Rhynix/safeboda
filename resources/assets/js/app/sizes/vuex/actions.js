import Bus from '../../../bus'

export const fetchAllallpromocodes = ({ commit }, { payload }) => {
    return axios.get('/api/allpromos?page=' + payload.page).then((response) => {
        commit('PrevOrdersPage', payload.page)
        commit('setAllOrdersData', response.data.data.data)
        commit('setOrdersMeta', response.data)
    })
}

export const addPromoCode = ({ commit }, { payload, context }) => {
    return axios.post('/api/newcodes', payload).then((response) => {
        context.newData = response.data.data
    }).catch((error) => {
        context.errors = error.response.data.errors
   })
}

export const selectedPromo = ({ commit }, data) => {
    commit('selectedPromo', data)
}

export const updatSelectPromo = ({ dispatch, commit }, { payload, context }) => {
    return axios.put('/api/promocode/' + payload.id + '/update', payload).then((response) => {
    }).catch((error) => {
        context.errors = error.response.data.errors
   })
}
















export const fetctSizelist = ({ commit }) => {
    return axios.get('/api/sizelist').then((response) => {
        commit('setsizelist', response.data.data)
    })
}
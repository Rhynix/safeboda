import { Login, Register, ForgotPassword } from '../components'

export default [
    {
        path: '/login',
        component: Login,
        name: 'login',
        meta: {
            guest: true,
            needsAuth: false
        }
    },
    {
        path: '/iforgot',
        component: ForgotPassword,
        name: 'iforgot',
        meta: {
            guest: true,
            needsAuth: false
        }
    },
    {
        path: '/register',
        component: Register,
        name: 'register',
        meta: {
            guest: true,
            needsAuth: false
        }
    }
]
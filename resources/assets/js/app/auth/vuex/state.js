export default {
    user: {
        authenticated: false,
        data: null
    },
	password: [],
	hostlink: 'http://dashboard.photozuri.com/img/users',
	// thumblink: 'http://uboldphotozuri.io:8888/img/thumb',
	thumblink: 'http://dashboard.photozuri.com/public/img/thumb',
	productlink: 'http://dashboard.photozuri.com/public/img/products',
	userscount: null,
	prosorders: null,
	completeorders: null,
	deliveredorders: null
}
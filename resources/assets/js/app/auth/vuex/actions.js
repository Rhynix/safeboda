import { setHttpToken } from '../../../helpers'
import { isEmpty } from 'lodash'
import localforage from 'localforage'

export const login = ({ dispatch }, { payload, context }) => {
    return axios.post('/api/auth/login', payload).then((response) => {
        dispatch('setToken', response.data.meta.token).then(() => {
            dispatch('fetchUser')
        })
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const register = ({ dispatch }, { payload, context}) => {
    return axios.post('/api/user/register', payload).then((response) => {
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const logout = ({ dispatch }, { payload }) => {
    return axios.post('/api/auth/logout', payload).then((response) => {
        dispatch('clearAuth')
    })
}

export const resetpassword = ({ dispatch }, { payload, context}) => {
    return axios.post('/api/auth/password/reset', payload).then((response) => {
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const fetchUser = ({ dispatch, commit }) => {
    return axios.get('/api/auth/user').then((response) => {
        commit('setAuthenticated', true)
        commit('setUserData', response.data.data)
        dispatch('usersCount')
    })
}

export const usersCount = ({ commit }) => {
    return axios.get('/api/usercount').then((response) => {
        commit('setUsersCount', response.data.data)
    })
}

export const setToken = ({ commit, dispatch }, token) => {
    if (isEmpty(token)) {
        return dispatch('checkTokenExists').then((token) => {
            setHttpToken(token)
        })
    }
    commit('setToken', token)
    setHttpToken(token)
}


export const checkTokenExists = ({ commit, dispatch }, token) => {
    return localforage.getItem('authtoken').then((token) => {
        if (isEmpty(token)) {
            return Promise.reject('NO_STORAGE_TOKEN');
        }
        return Promise.resolve(token)
    })
}

export const clearAuth = ({ commit }, token) => {
    commit('setAuthenticated', false)
    commit('setUserData', null)
    commit('setToken', null)
    setHttpToken(null)
}


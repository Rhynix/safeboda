export const user = (state) => {
    return state.user
}

export const password = (state) => {
    return state.password
}

export const hostlink = (state) => {
    return state.hostlink
}

export const thumblink = (state) => {
    return state.thumblink
}

export const productlink = (state) => {
    return state.productlink
}

export const getuserscount = (state) => {
    return state.userscount
}

export const getprosorders = (state) => {
    return state.prosorders
}

export const getcompleteorders = (state) => {
    return state.completeorders
}

export const getdeliveredorders = (state) => {
    return state.deliveredorders
}
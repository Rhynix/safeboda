import localforage from 'localforage'
import {isEmpty} from 'lodash'

export const setToken = (state, token) => {
	if (isEmpty(token)) {
		localforage.removeItem('authtoken', token)
		return
	}
	localforage.setItem('authtoken', token)
}

export const setAuthenticated = (state, trueOrFalse) => {
	state.user.authenticated = trueOrFalse
}

export const setUserData = (state, data) => {
	state.user.data = data
}

export const setUsersPassword = (state, data) => {
	state.password = data
}

export const setUsersCount = (state, data) => {
	state.userscount = data
}

export const setProsOrderCount = (state, data) => {
	state.prosorders = data
}

export const setCompleteOrderCount = (state, data) => {
	state.completeorders = data
}

export const setDeliveredOrderCount = (state, data) => {
	state.deliveredorders = data
}
import Bus from '../../../bus'

export const getpolyline = ({ commit }, { payload, context }) => {
    return axios.post('/api/promocodevalidity', payload).then((response) => {
        commit('setPolyLineData', response.data)
    }).catch((error) => {
        error.response.status >= 500 ? context.statusCode = true : context.errors = error.response.data.errors
    })
}
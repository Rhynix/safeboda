import { Promocheck } from '../components'

export default [
    {
        path: '/promocheck',
        component: Promocheck,
        name: 'promocheck',
        meta: {
            needsAuth: true
        }
    }
]

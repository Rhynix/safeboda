import { Products, Newproduct, Editproduct } from '../components'

export default [
    {
        path: '/products',
        component: Products,
        name: 'products',
        meta: {
            needsAuth: true
        }
    },
    {
        path: '/new-products',
        component: Newproduct,
        name: 'new-products',
        meta: {
            needsAuth: true
        }
    },
    { name: "edit-product.edit", path: '/edit-product/:id/edit', component: Editproduct, meta: { needsAuth: true }  }
]

import moment from 'moment'

export const getprevPage = (state) => {
    return state.prevPage
}

export const getorders = (state) => {
    return state.orders
}

export const getvenuelists = (state) => {
    return state.venuelists
}

export const getsize = (state) => {
    return state.order
}

export const getproduct = (state) => {
    return state.product
}

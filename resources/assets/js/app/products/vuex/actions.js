import Bus from '../../../bus'

export const selectedSize = ({ commit }, data) => {
    commit('selectedSize', data)
}

export const fetctAllorders = ({ commit }, { payload }) => {
    return axios.get('/api/allevent?page=' + payload.page).then((response) => {
        commit('setAllOrdersData', response.data)
    })
}

export const fetchVenueList = ({ commit }) => {
    return axios.get('/api/alleventlist').then((response) => {
        commit('setVenueLists', response.data.data)
    })
}

export const searchVenue = ({ commit }, page ) => {
    return axios.get('/api/searchvenue/search?search=' + page[1] + '&page=' + page[0] ).then((response) => {
        commit('setAllOrdersData', response.data)
    })
}

export const getSelectEvent = ({ commit }, { payload }) => {
    return axios.get('/api/getevent/'+payload.id).then((response) => {
        commit('setSelectProduct', response.data.data)
    })
}

export const updatSelectEvent = ({ dispatch, commit }, { payload, context }) => {
    return axios.put('/api/event/' + payload.id + '/update', payload).then((response) => {
    }).catch((error) => {
        context.errors = error.response.data.errors
   })
}

export const addVenue = ({ commit }, { payload, context }) => {
    return axios.post('/api/newevent', payload).then((response) => {
    }).catch((error) => {
        context.errors = error.response.data.errors
   })
}
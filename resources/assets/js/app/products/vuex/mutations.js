import moment from 'moment'

export const PrevOrdersPage = (state, data) => {
    state.prevPage = data
}

export const setAllOrdersData = (state, data) => {
	state.orders = data
}

export const setVenueLists = (state, data) => {
	state.venuelists = data
}

export const selectedSize = (state, data) => {
	state.order = data
}

export const setSelectProduct = (state, data) => {
	state.product = data
}
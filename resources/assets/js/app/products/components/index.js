import Vue from 'vue'

export const Products = Vue.component('products', require('./Products.vue'))
export const Newproduct = Vue.component('newproduct', require('./Newproduct.vue'))
export const Editproduct = Vue.component('editproduct', require('./Editproduct.vue'))
export const Productphoto = Vue.component('product-photo', require('./Productphoto.vue'))
import Bus from '../../../bus'

export const getP001pdf = ({ commit }, { payload, context }) => {
    return axios.post('/api/validate', payload).then((response) => {
        if (response.data.data == 'Ok') {
            axios({
                method:'post',
                url:'/api/P001pdf',
                responseType:'arraybuffer',
                data: payload
            }).then(function(response) {
                let blob = new Blob([response.data], { type: 'application/pdf' } )
                let link = document.createElement('a')
                link.href = window.URL.createObjectURL(blob)
                link.download = 'Orders made in a given period.pdf'
                link.click()
                context.PZ1 = false
            });
        }
    }).catch((error) => {
        context.errors = error.response.data.errors;
        context.PZ1 = false
    })
}

export const getP002pdf = ({ commit }, { payload, context }) => {
    return axios.post('/api/validate', payload).then((response) => {
        if (response.data.data == 'Ok') {
            axios({
                method:'post',
                url:'/api/P002pdf',
                responseType:'arraybuffer',
                data: payload
            }).then(function(response) {
                let blob = new Blob([response.data], { type: 'application/pdf' } )
                let link = document.createElement('a')
                link.href = window.URL.createObjectURL(blob)
                link.download = 'All Orders grouped by category.pdf'
                link.click()
                context.PZ2 = false
            });
        }
    }).catch((error) => {
        context.errors = error.response.data.errors;
        context.PZ2 = false
    })
}
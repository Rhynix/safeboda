import moment from 'moment'

export const PrevOrdersPage = (state, data) => {
    state.prevPage = data
}

export const setAllOrdersData = (state, data) => {
	state.orders = data
}

export const setOrdersMeta = (state, data) => {
	state.ordersmeta = data
}

export const selectedSize = (state, data) => {
	state.order = data
}

export const setsizelist = (state, data) => {
	state.sizelist = data
}


import moment from 'moment'

export const getprevPage = (state) => {
    return state.prevPage
}

export const getorders = (state) => {
    return state.orders
}

export const getordersmeta = (state) => {
    return state.ordersmeta
}

export const getsize = (state) => {
    return state.order
}

export const getsizelist = (state) => {
    return state.sizelist
}
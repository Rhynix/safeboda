import auth from './auth/routes'
import errors from './errors/routes'
import users from './users/routes'
import home from './home/routes'
import sizes from './sizes/routes'
import products from './products/routes'
import promocheck from './promocheck/routes'

export default [
  ...auth,
  ...errors,
  ...users,
  ...home,
  ...sizes,
  ...products,
  ...promocheck
]

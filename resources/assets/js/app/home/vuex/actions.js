import Bus from '../../../bus'

export const changeDate = ({ commit }, data) => {
    commit('changeDate', data)
}

export const addCallback = ({ commit }, data) => {
    commit('addCallback', data)
}

export const changeWeekDate = ({ commit }, payload) => {
    commit('changeWeekDate', payload)
}
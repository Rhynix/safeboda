import moment from 'moment'
export const changeDate = (state, data) => {
	state.date = moment(data)
}

export const addCallback = (state, data) => {
	state.callbacks.push(data)
}

export const deleteCallback = (state, data) => {
	state.callbacks = state.callbacks.filter((callback) => {
		return callback.id !== data
	})
}

export const setSelectCallback = (state, data) => {
    state.call = data
}

export const changeWeekDate = (state, data) => {
	state.weekDate = moment(data)
}

export const setCallbacksData = (state, data) => {
	state.callbacks = data
}

export const setWeekCallbacks = (state, data) => {
	state.weekCallbacks = data
}

export const setCallbacksHistory = (state, data) => {
	state.history = data
}

export const setCallbacksMeta = (state, data) => {
	state.meta = data
}

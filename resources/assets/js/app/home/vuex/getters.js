import moment from 'moment'

export const getDate = (state) => {
    return state.date
}
export const getWeekDate = (state) => {
    return state.weekDate
}
export const getDateFormatted = (state) => {
    return state.date.calendar(null , {
        sameDay: '[Today]',
        nextDay: '[Tomorrow]',
        nextWeek: 'dddd',
        lastDay: '[Yesterday]',
        lastWeek: '[Last] dddd',
        sameElse: 'DD MMM, YYYY'
    })
}

export const getCall = (state) => {
    return state.call
}
export const getCallbacks = (state) => {
    return state.callbacks
}
export const getWeekCallbacks = (state) => {
    return state.weekCallbacks
}
export const getCallbacksHistory = (state) => {
    return state.history
}
export const getMeta = (state) => {
    return state.meta
}

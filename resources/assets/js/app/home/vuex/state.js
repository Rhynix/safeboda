import moment from 'moment'
export default {
	date: moment(),
	weekDate: moment(),
	callbacks: [],
	weekCallbacks: [],
	history: [],
	meta: null,
	call: null
}

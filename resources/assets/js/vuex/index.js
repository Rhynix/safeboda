import Vue from 'vue'
import Vuex from 'vuex'
import auth from '../app/auth/vuex'
import users from '../app/users/vuex'
import home from '../app/home/vuex'
import sizes from '../app/sizes/vuex'
import products from '../app/products/vuex'
import promocheck from '../app/promocheck/vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
      auth: auth,
      users: users,
      home: home,
      sizes: sizes,
      products: products,
      promocheck: promocheck
    }
})

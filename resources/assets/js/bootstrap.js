// import Popper from 'popper.js/dist/umd/popper.js';
window._ = require('lodash');

// require('../../../public/assets/js/jquery.js')
try {
     window.$ = window.jQuery = require('jquery');
     window.Popper = Popper;
     require('bootstrap');
} catch (e) {}

window.swal = require('sweetalert2');
window.moment = require('moment');
require('moment/locale/en-gb');
require('moment-timezone')
moment().locale('en-gb')
moment.tz.setDefault("Africa/Nairobi");

require('../../../public/assets/js/jquery.min.js')
require('../../../public/assets/js/jquery.core.js')
require('../../../public/assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js')
require('../../../public/assets/js/jquery-2.1.3.min.js')
require('../../../public/assets/js/markerclusterer.js')
/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from "laravel-echo"

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: Laravel.keys.pusher,
    cluster: 'eu',
    encrypted: true
});


require('./echo')

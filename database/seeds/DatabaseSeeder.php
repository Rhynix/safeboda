<?php

use Illuminate\Database\Seeder;
use App\Models\User\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = bcrypt(config('app.adminpass'));
		$users = [
			[
			'firstname' => 'John',
			'lastname' => 'Wanyoike',
			'email' => 'jhnwanyoike@gmail.com',
			'password' => $password,
			'status' => 1,
			'role_id' => 1
			],
			[
			'firstname' => 'Eddy',
			'lastname' => 'Programmer',
			'email' => 'eddy@gmail.com',
			'password' => $password,
			'status' => 2,
			'role_id' => 2
			],
		];

		foreach ($users as $user){
			User::create($user);
		}
    }
}
